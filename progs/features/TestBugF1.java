class MinimalCode {
    public static void main(String[] args) {
        System.out.println(new Simple().test1());
    }
}

class Simple {
    public int test1() {
    int result;
    int x;
    int y;
    x = 6;
    y = 6;
    result = 0;
    //Checks whether two integers can be OR'd.
    if(x || y){
      result = result + 1;
    }else{
      result = result - 1;
    }
    return result;
  }

}
