class MinimalCode {
    public static void main(String[] args) {
        System.out.println(new Simple().test2());
    }
}

class Simple {
  public int test2() {
    boolean x;
    boolean y;
    int result;
    x = true;
    y = false;
    result = 0;
    //Checks if "true || false" will return true.
    if(x || y) {
      result = result + 1;
    }
    else {
      result = result - 1;
    }
    return result;
  }
