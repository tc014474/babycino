class MinimalCode {
    public static void main(String[] args) {
        System.out.println(new Simple().test3());
    }
}

class Simple {
    public int test3() {
      boolean x;
      boolean y;
      int result;
      x = false;
      y = true;
      result = 0;
      //Compares "false || true" to return true;
    if(x || y) {
      result = result + 1;
    }
    else{
      result = result + 0;
    }
    return result;
  }

}
